Qemu packages for CNRL repositories
===================================

The repository uses gitlab-ci to build the source packages of Qemu for
different Ubuntu distributions.

Each job in the pipeline will create a source package for Qemu with the
support for VDE. This package is then signed [1]_ and uploaded to
Launchpad [2]_, that is responsible to build the actual binary packages.
The binary packages are then manually uploaded to cnrl.deis.unibo.it.


The process creates the following artifacts:

- qemu_*vde*.debian.tar.xz
- qemu_*vde*.dsc
- qemu_*vde*_source.build
- qemu_*vde*_source.buildinfo
- qemu_*vde*_source.changes

Gitlab must be configured with two secure files (``public.key`` and
``private.key``) with the GPG key used to sign the package.

.. [1] The key used to sign the packages is
    C24E295A81C93B4E3E1827B33B2C9DB65D13F6BC
.. [2] https://launchpad.net/~marco-giusti/+archive/ubuntu/qemu/
